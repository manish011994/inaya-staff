<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'LoginController@showHome');
Route::get('/login', 'LoginController@showLogin');
Route::get('/register', 'UserController@showRegister');

Route::group(['middleware' => 'throttle:4,1'], function () {
    Route::get('/verify-otp', 'UserController@viewVerifyOTP');
    Route::post('/resend-otp', 'UserController@resendOTP');
    Route::post('/verify-otp', 'UserController@verifyOTP');
    Route::post('/forgot-password', 'LoginController@forgotPassword');
    Route::post('/forgot-password/resend-otp', 'LoginController@resendForgotPassword');
});

Route::post('/is-phone-registered', 'UserController@isPhoneRegistered');

Route::post('/is-email-registered', 'UserController@isEmailRegistered');

Route::get('/forgot-password', 'LoginController@showForgotPassword');

Route::get('/reset-password', 'LoginController@showResetPassword');

Route::post('/reset-password', 'LoginController@resetPassword');

Route::get('/fetch_unit_data', 'UnitMasterController@getUnitIds');

Route::get('/fetch-cust-update-request', 'UnitMasterController@getCustUpdateRequest');

// Only 5 times login allowed in a minute
Route::group(['middleware' => 'throttle:5,1'], function () {
    Route::post('/login', 'LoginController@login');
    Route::post('/register', 'UserController@register');
});

Route::middleware('checkLogin')->group(function () {
    Route::get('/new-request', 'RequestController@newRequest');

    Route::post('/get-category', 'FailureClassController@getFailureClass');

    Route::get('/list-request/placed-requests', 'RequestController@listPlacedRequest');

    Route::get('/list-request/update-requests', 'RequestController@listUpdateRequest');

    Route::get('/list-request/get-update-requests', 'RequestController@getUpdateRequest');

    Route::middleware('isAdmin')->group(function () {
        Route::prefix('/nature-of-complaints')->group(function () {
            Route::get('/', 'NatureComplaintController@manageNatureComplaint');
            Route::get('/add', function () {
                return view('nature_of_complaints.add');
            });
            Route::post('/add', 'NatureComplaintController@addNoc');
            Route::get('/edit/{nocId}', 'NatureComplaintController@showNoc')->where('nocId', '[0-9]+');
            Route::post('/edit/{nocId}', 'NatureComplaintController@updateNoc')->where('nocId', '[0-9]+');

            Route::post('/delete/{nocId}', 'NatureComplaintController@deleteNoc')->where('nocId', '[0-9]+');
        });


        Route::prefix('/category-failure-class')->group(function () {
            Route::get('/', 'CategoryController@manageCategory');
            Route::get('/add', function () {
                return view('category_failure_class.add');
            });
            Route::post('/add', 'CategoryController@addCategory');
            Route::get('/edit/{categoryId}', 'CategoryController@showCategory')->where('categoryId', '[0-9]+');
            Route::post('/edit/{categoryId}', 'CategoryController@updateCategory')->where('categoryId', '[0-9]+');

            Route::post('/delete/{categoryId}', 'CategoryController@deleteCategory')->where('categoryId', '[0-9]+');

            Route::prefix('/failure-class/{categoryId}')->group(function () {
                Route::get('/', 'FailureClassController@manageFailureClass')->where('categoryId', '[0-9]+');
                Route::get('/add', 'FailureClassController@addFailureClassPage')->where('categoryId', '[0-9]+');
                Route::post('/add', 'FailureClassController@addFailureClass')->where('categoryId', '[0-9]+');
                Route::get('/edit/{failureClassId}', 'FailureClassController@showFailureClass')->where('categoryId', '[0-9]+')->where('failureClassId', '[0-9]+');
                Route::post('/edit/{failureClassId}', 'FailureClassController@updateFailureClass')->where('categoryId', '[0-9]+')->where('failureClassId', '[0-9]+');

                Route::post('/delete/{failureClassId}', 'FailureClassController@deleteFailureClass')->where('failureClassId', '[0-9]+');
            });
        });

        Route::prefix('/phone-change-request')->group(function() {
            Route::get('/', 'PhoneChangeRequestController@managePhoneChangeRequest');
            Route::post('/{requestId}/approve', 'PhoneChangeRequestController@approveRequest')->where('requestId', '[0-9]+');
            Route::post('/{requestId}/reject', 'PhoneChangeRequestController@rejectRequest')->where('requestId', '[0-9]+');
        });
    });

    Route::get('/profile-update', function () {
        return view('profile_update', ['userDetails' => session()->get('user_details')]);
    });
    Route::post('/profile-update', 'UserController@updateProfile');

    Route::get('/change-password', function () {
        return view('change-password');
    });
    Route::post('/change-password', 'UserController@changePassword');

    Route::get('/logout', 'LoginController@logout');

    Route::post('/new_cust_request', 'RequestController@submitRequest');

    Route::get('/contact-us', 'LoginController@contactUs');

});

/*
 *
 */
