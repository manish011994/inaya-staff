<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NatureComplaintModel extends Model
{
    protected $table = 'nature_of_complaints';

    public static function getComplaintByName($name, $nocInitials)
    {
        return NatureComplaintModel::where('name', $name)
            ->orWhere('initials', $nocInitials)
            ->where('status', 'Y')->first();
    }
}
