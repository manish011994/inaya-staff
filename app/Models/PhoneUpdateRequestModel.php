<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneUpdateRequestModel extends Model
{
    protected $table = 'phone_update_request';

    public static function isAlreadyRequested($userDetails)
    {
        return PhoneUpdateRequestModel::where('user_id', $userDetails->id)
            ->where('status', 'Pending')
            ->first();
    }
}
