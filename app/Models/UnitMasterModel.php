<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnitMasterModel extends Model
{
    protected $table = 'unit_master';

    public static function isUnitIdPresent($unitId, $contractNo) {
        return UnitMasterModel::where('unit_id', $unitId)->where('contract_no', $contractNo)->first();
    }
}
