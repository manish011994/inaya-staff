<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected $table = 'categories';

    public static function getCategoryByName($name)
    {
        return CategoryModel::where('category', $name)->where('parent_id', 0)->where('status', 'Y')->first();
    }

    public static function getFailureClassByName($name, $categoryId)
    {
        return CategoryModel::where('category', $name)
            ->where('parent_id', $categoryId)
            ->where('status', 'Y')->first();
    }
}
