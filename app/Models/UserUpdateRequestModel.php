<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserUpdateRequestModel extends Model
{
    protected $table = 'user_update_requests';

    public static function isComplaintPresent($complaintNo, $createdDate)
    {
        return UserUpdateRequestModel::where('complaint_no', $complaintNo)->where('created_date', $createdDate)->first();
    }
}
