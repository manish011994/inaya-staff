<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    protected $table = 'staff_users';

    public static function getUserByPhone($phone)
    {
        return User::select('phone')
            ->where('phone', $phone)
            ->first();
    }

    public static function getUserByEmail($email)
    {
        return User::select('email_id')
            ->where('email_id', $email)
            ->first();
    }

    public static function getUserByEmailAndPhone($email, $phone)
    {
        return User::select('email_id')
            ->where('email_id', $email)
            ->orWhere('phone', $phone)
            ->first();
    }

    public static function verifyUser($phone, $pass)
    {
        return User::where('phone', $phone)
            ->where('password', sha1($pass))
            ->first();
    }
}
