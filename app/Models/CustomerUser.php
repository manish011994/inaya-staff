<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerUser extends Model
{
    protected $table = 'users';

    public static function getUser($user_id)
    {
        return CustomerUser::where('id', $user_id)->first();
    }
}
