<?php

use App\Models\UnitMasterModel;
use App\Models\UserRequestLog;
use App\Models\UserUpdateRequestModel;
use GuzzleHttp\Client;
use League\Csv\Reader;
use League\Csv\Writer;

/**
 * @param $columnNames
 * @param $rows
 * @param string $fileName
 * Creating new CSV as per requirement
 */
function createCSV($columnNames, $rows, $fileName = 'file.csv')
{
    $csvFile = fopen($fileName, "w") or die("Unable to open file");
    fwrite($csvFile, implode(",", $columnNames));
    fwrite($csvFile, "\n");
    fwrite($csvFile, implode(",", $rows));
    fclose($csvFile);
    return true;
}

/*function createCSV($columnNames, $rows, $fileName = 'file.csv')
{
    try {
        $rows = array($rows); // For making iterable
        $writer = Writer::createFromPath($fileName, 'w+');
        $writer->insertOne((array)$columnNames);
        $writer->insertAll(new ArrayIterator($rows)); //using a Traversable object
        return $writer;
    } catch (League\Csv\Exception $exception) {
        return $exception->getMessage();
    }
}*/

function storeUnitMasterDataFromCSV($files, $context)
{
    if (!empty($files) && is_array($files) && count($files)) {
        foreach ($files as $file) {
            //load the CSV document from a file path
            $csv = Reader::createFromPath(env('DOWNLOAD_UNIT_MASTER_PATH') . $file['filename'], 'r');
            $csv->setHeaderOffset(0);

            $header = $csv->getHeader(); //returns the CSV header record
            $records = $csv->getRecords(); //returns all the CSV records as an Iterator object
            foreach ($records as $record) {
                $isUnitIdPresent = UnitMasterModel::isUnitIdPresent($record['Unit'], $record['ContractNo']);
                if (empty($isUnitIdPresent)) {
                    $unitData = new UnitMasterModel();
                    $unitData->unit_id = $record['Unit'];
                    $unitData->serial_no = $record['SerialNo'];
                    $unitData->contact_phone = !empty($record['ContactPhone']) ? substr($record['ContactPhone'], -9) : "";
                    $unitData->end_customer = $record['EndCustomer'];
                    $unitData->bill_to_customer = $record['BillToCustomer'];
                    $unitData->contract_no = $record['ContractNo'];
                    $unitData->item_code = $record['ItemCode'];
                    $unitData->cost_center = $record['CostCenter'];
                    $unitData->location = $record['Location'];
                    $unitData->end_customer_address_id = $record['EndCustomerAddressID'];
                    $unitData->bill_to_customer_address_id = $record['BillToCustomerAddressID'];
                    $unitData->installed_date = $record['InstalledDate'];
                    $unitData->unit_category = $record['UnitCategory'];
                    if ($unitData->save()) {
                        $context->info("Unit Saved - " . $unitData->unit_id);
                    }
                } else {
                    $context->info("Unit Id already present - " . $record['Unit'] . '-' . $record['ContractNo']);
                }
            }
        }
    }
}

function storeUserUpdateRequestDataFromCSV($files, $context)
{
    if (!empty($files) && is_array($files) && count($files)) {
        foreach ($files as $file) {
            //load the CSV document from a file path
            $csv = Reader::createFromPath(env('DOWNLOAD_CUST_UPDATE_REQUEST_PATH') . $file['filename'], 'r');
            $csv->setHeaderOffset(0);

            $header = $csv->getHeader(); //returns the CSV header record
            $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

            foreach ($records as $record) {
                $isComplaintPresent = UserUpdateRequestModel::isComplaintPresent($record['ComplaintNo'], $record['CreatedDate']);
                if (empty($isComplaintPresent)) {
                    $userUpdateRequest = new UserUpdateRequestModel();
                    $userUpdateRequest->complaint_no = $record['ComplaintNo'];
                    $userUpdateRequest->swo_description = $record['SWODescription'];
                    $userUpdateRequest->project_cost_center = $record['ProjectCostCenter'];
                    $userUpdateRequest->unit_id = $record['UnitId'];
                    $userUpdateRequest->category = $record['Category'];
                    $userUpdateRequest->status = $record['Status'];
                    $userUpdateRequest->created_date = $record['CreatedDate'];
                    $userUpdateRequest->created_time = $record['CreatedTime'];
                    $userUpdateRequest->priority = $record['Priority'];
                    $userUpdateRequest->problem_code = $record['ProblemCode'];
                    $userUpdateRequest->customer_code = $record['CustomerCode'];
                    $userUpdateRequest->appointment_date = $record['AppointmentDate'];
                    $userUpdateRequest->appointment_slot = $record['AppointmentSlot'];
                    $userUpdateRequest->appointment_remarks = $record['AppointmentRemarks'];
                    $userUpdateRequest->customer_name = $record['CustomerName'];
                    $userUpdateRequest->customer_phone = !empty($record['CustomerPhone']) ? substr($record['CustomerPhone'], -9) : "";
                    $userUpdateRequest->secondary_name = $record['SecondaryName'];
                    $userUpdateRequest->secondary_phone_no = !empty($record['SecondaryPhoneno']) ? substr($record['SecondaryPhoneno'], -9) : "";
                    $userUpdateRequest->internal_status = $record['InternalStatus'];
                    $userUpdateRequest->swo_type = $record['SWOType'];
                    $userUpdateRequest->client_sr_no = $record['ClientSRNo'];
                    $userUpdateRequest->work_procedure_last_update = $record['WorkProcedureLastUpdate'];
                    $userUpdateRequest->assign_to = $record['AssignTo'];
                    $userUpdateRequest->work_procedure_last_status = $record['WorkProcedureLastStatus'];
                    if ($userUpdateRequest->save()) {
                        //TODO: SMS (Need Confirmation)
                        /*if (trim($userUpdateRequest->status) == 'Job Completed') {
                            // Send Email
                        }*/
                        $context->info("Complaint Saved - " . $userUpdateRequest->complaint_no);
                    }
                } else {
//                    \Illuminate\Support\Facades\Mail::to(env('SERVER_ADMIN_EMAIL'))->queue(new \App\Mail\EmailForQueuing());
                    $context->info("Complaint Id already present - " . $record['ComplaintNo'] . '-' . $record['Status']);
                }
            }
        }
    }
}

function createLog($email, $url, $processName, $request, $response)
{
    $log = new UserRequestLog();
    $log->email = $email;
    $log->url = $url;
    $log->process_name = $processName;
    $log->request = json_encode($request);
    $log->response = json_encode($response);
    $log->save();
}

function getSFTPError($error)
{
    if (!empty($error) && str_contains($error, ":")) {
        return trim(explode(":", $error)[1]);
    } else {
        return "Something went wrong with SFTP Server Connection";
    }
}

function verifyCaptcha($request)
{
    $endpoint = "https://www.google.com/recaptcha/api/siteverify";
    $client = new Client();
    $response = $client->request('POST', $endpoint, ['query' => [
        'secret' => getenv('GOOGLE_RECAPTCHA_SECRET_KEY'),
        'response' => $request->get('g-recaptcha-response'),
        'remoteip' => $request->ip()
    ]]);
    $content = json_decode($response->getBody(), true);
    return $content['success'];
}

function sendSMS($phone, $message)
{
    $mobilenumbers = "971" . $phone;
    $endpoint = "http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
    $client = new Client();
    $response = $client->request('POST', $endpoint, ['query' => [
        'User' => env('SMS_USERNAME'),
        'passwd' => env('SMS_PASSWORD'),
        'mobilenumber' => $mobilenumbers,
        'message' => $message,
        'sid' => env('SMS_SENDER_ID'),
        'mtype' => 'N',
        'DR' => 'Y'
    ]]);
    $content = json_decode($response->getBody(), true);
    return $content;
}

if (!function_exists('r_print')) {
    function r_print($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}

function removeSpecialCharacters($data)
{
    foreach ($data as $key=>$value) {
        if(stripos($key, "email") === FALSE) {
            $data[$key] = str_replace(array('“', ';', '<', '>', '#', '!', '$', '%', '^', '&', '*', '(', ')', '}',
                '{', '[', ']', '?', '/', ',', '\\', '|', '+', '.', '"', "'", "_", ":"), "-", $value);
        }
    }
    return $data;
}
