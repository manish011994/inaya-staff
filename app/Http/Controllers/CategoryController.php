<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function manageCategory(Request $request)
    {
        $categories = CategoryModel::where('status', 'Y')->where('parent_id', 0)->get();
        return view('category_failure_class.manage_category_failure_class', ['categories' => $categories]);
    }

    public function addCategory(Request $request)
    {
        $result = array();
        $categoryName = $request->get('category');
        $isCategoryAdded = CategoryModel::getCategoryByName($categoryName);
        if (empty($isCategoryAdded->category)) {
            $category = new CategoryModel();
            foreach ($request->all() as $key => $value) {
                if (!in_array($key, ['_token'])) {
                    $category->$key = $value;
                }
            }
            if ($category->save()) {
                $result['success'] = true;
                $result['msg'] = $categoryName . " added.";
            } else {
                $result['success'] = false;
                $result['msg'] = "Error in saving Category.";
            }
        } else {
            $result['success'] = false;
            $result['msg'] = $categoryName . " is already added.";
        }
        return json_encode($result);
    }

    public function showCategory($categoryId)
    {
        $category = CategoryModel::where('id', $categoryId)->where('parent_id', 0)->where('status', 'Y')->first();
        if (!empty($category)) {
            return view('category_failure_class.add', ['category' => $category]);
        } else {
            return abort(404);
        }
    }

    public function updateCategory(Request $request, $categoryId)
    {
        $result = array();
        $category = CategoryModel::where('id', $categoryId)->where('parent_id', 0)->where('status', 'Y')->first();
        if (!empty($category)) {
            $category->category = $request->get('category');
            if ($category->save()) {
                $result['success'] = true;
                $result['msg'] = $category->category . " updated.";
            } else {
                $result['success'] = false;
                $result['msg'] = "Error in updating.";
            }
            return json_encode($result);
        } else {
            return abort(404);
        }
    }

    public function deleteCategory($categoryId)
    {
        $category = CategoryModel::where('id', $categoryId)->where('parent_id', 0)->where('status', 'Y')->first();
        if (!empty($category)) {
            $category->status = 'N';
            if ($category->save()) {
                return redirect('/category-failure-class');
            }
        } else {
            return abort(404);
        }
    }
}
