<?php

namespace App\Http\Controllers;

use App\Models\NatureComplaintModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NatureComplaintController extends Controller
{
    public function manageNatureComplaint(Request $request)
    {
        $nocs = NatureComplaintModel::where('status', 'Y')->get();
        return view('nature_of_complaints.manage_nature_of_complaints', ['nocs' => $nocs]);
    }

    public function addNoc(Request $request)
    {
        $result = array();
        $nocName = $request->get('name');
        $nocInitials = $request->get('initials');
        $isNocAdded = NatureComplaintModel::getComplaintByName($nocName, $nocInitials);
        if (empty($isNocAdded->name)) {
            $user = new NatureComplaintModel();
            foreach ($request->all() as $key => $value) {
                if (!in_array($key, ['_token'])) {
                    $user->$key = $value;
                }
            }
            if ($user->save()) {
                $result['success'] = true;
                $result['msg'] = $nocName . " added.";
            } else {
                $result['success'] = false;
                $result['msg'] = "Error in saving Nature of Complaint.";
            }
        } else {
            $result['success'] = false;
            $result['msg'] = $nocName . " / " . $nocInitials ." is already added.";
        }
        return json_encode($result);
    }

    public function showNoc($nocId)
    {
        $noc = NatureComplaintModel::where('id', $nocId)->first();
        if (!empty($noc)) {
            return view('nature_of_complaints.add', ['noc' => $noc]);
        } else {
            return abort(404);
        }
    }

    public function updateNoc(Request $request, $nocId)
    {
        $result = array();
        $noc = NatureComplaintModel::where('id', $nocId)->first();
        if (!empty($noc)) {
            $noc->name = $request->get('name');
            $noc->initials = $request->get('initials');
            if ($noc->save()) {
                $result['success'] = true;
                $result['msg'] = $noc->name . " updated.";
            } else {
                $result['success'] = false;
                $result['msg'] = "Error in updating.";
            }
            return json_encode($result);
        } else {
            return abort(404);
        }
    }

    public function deleteNoc($nocId)
    {
        $noc = NatureComplaintModel::where('id', $nocId)->first();
        if (!empty($noc)) {
            $noc->status = 'N';
            if ($noc->save()) {
                return redirect('/nature-of-complaints');
            }
        } else {
            return abort(404);
        }
    }
}
