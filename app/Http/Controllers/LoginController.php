<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLogin(Request $request)
    {
        if (session()->has('user_details')) {
            return redirect('/');
        } else if (session()->has('user_register')) {
            return redirect('/verify-otp');
        } else {
            return view('login');
        }
    }

    public function login(Request $request)
    {
        $result = array();
        $verifyCaptcha = verifyCaptcha($request);
        if ($verifyCaptcha) {
            $username = (int) $request->get('phone');
            $password = $request->get('password');
            if (!empty($username) && !empty($password)) {
                $verifyUser = User::verifyUser($username, $password);
                if (!empty($verifyUser)) {
                    $request->session()->regenerate();
                    if ($verifyUser->mobile_verify_status == 'N') {
                        session(['user_register' => $verifyUser]);
                    } else {
                        session(['user_details' => $verifyUser]);
                    }
                    $result['success'] = true;
                    $result['msg'] = "Logged In Successfully";
                } else {
                    $result['success'] = false;
                    $result['msg'] = "Invalid Username or Password.";
                }
            } else {
                $result['success'] = false;
                $result['msg'] = "Username or Password should not be empty.";
            }
        } else {
            $result['success'] = false;
            $result['msg'] = "Invalid Captcha Response";
        }
        return json_encode($result);
    }

    public function showHome(Request $request)
    {
        if (session()->has('user_details')) {
            return view('home');
        } else {
            return view('login');
        }
    }

    public function showForgotPassword(Request $request)
    {
        return view('forgot-password');
    }

    public function forgotPassword(Request $request)
    {
        $result = array();
        $verifyCaptcha = verifyCaptcha($request);
        if ($verifyCaptcha) {
            $phone = (int) $request->get('phone');
            $isPhonePresent = User::getUserByPhone($phone);
            if (!empty($isPhonePresent)) {
                $phone = $isPhonePresent->phone;
                $otp = rand(111111, 999999);
                $request->session()->put('otp', $otp);
                session()->put('phone', $phone);
                $sms = "Your Inaya Portal OTP Code for reset password is:" . $otp;
                sendSMS($phone, $sms);
                $result['success'] = true;
                $result['msg'] = 'OTP sent to your registered mobile number.';
            } else {
                $result['success'] = false;
                $result['msg'] = 'Phone number not registered';
            }
        } else {
            $result['success'] = false;
            $result['msg'] = 'Invalid reCaptcha response.';
        }
        return json_encode($result);
    }

    public function showResetPassword(Request $request)
    {
        if ($request->session()->has('otp') && $request->session()->has('phone')) {
            $phone = session()->get('phone');
            return view('reset-password', ['phone' => $phone]);
        } else {
            return redirect('/');
        }
    }

    public function resendForgotPassword(Request $request)
    {
        $result = array();
        if ($request->session()->has('otp') && $request->session()->has('phone')) {
            $sms = "Your Inaya Portal OTP Code for reset password is:" . $request->session()->get('otp');
            sendSMS(session()->get('phone'), $sms);
            $result['success'] = true;
            $result['msg'] = "OTP Sent.";
        } else {
            $result['success'] = false;
            $result['msg'] = "Invalid Request";
        }
        return json_encode($result);
    }

    public function resetPassword(Request $request)
    {
        $result = array();
        if (session()->has('otp') && session()->has('phone')) {
            $user = User::where('phone', session()->get('phone'))->first();
            if (!empty($user)) {
                if (session()->get('otp') == $request->post('otp')) {
                    if ($request->post('new_password') == $request->post('confirm_password') &&
                        !empty($request->post('new_password'))) {
                        $user->password = sha1($request->post('new_password'));
                        if ($user->save()) {
                            session()->pull('otp');
                            session()->pull('phone');
                            $result['success'] = true;
                            $result['msg'] = "Password reset successfully.";
                        } else {
                            $result['success'] = false;
                            $result['msg'] = "Some error occurred while changing the password.";
                        }
                    } else {
                        $result['success'] = false;
                        $result['msg'] = "New Passwords not matched.";
                    }
                } else {
                    $result['success'] = false;
                    $result['msg'] = "OTP entered is Invalid";
                }
            } else {
                $result['success'] = false;
                $result['msg'] = "Invalid Request, Please logout and login again.";
            }
            return json_encode($result);
        }
    }

    public function contactUs(Request $request)
    {
        return view('contact_us');
    }

    public function logout(Request $request)
    {
        $request->session()->forget(['user_details']);
        return redirect('/login');
    }
}
