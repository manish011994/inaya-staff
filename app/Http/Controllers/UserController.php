<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use phpseclib\Net\SFTP;

class UserController extends Controller
{
    public function showRegister(Request $request)
    {
        if (session()->has('user_details')) {
            return redirect('/');
        } else {
            return view('register');
        }
    }

    public function isPhoneRegistered(Request $request)
    {
        $result = array();
        $phone = (int) $request->post('phone');
        $isPhoneRegistered = User::getUserByPhone($phone);
        if (!empty($isPhoneRegistered)) {
            $result['success'] = true;
            $result['msg'] = "Phone no already registered.";
        }
        return json_encode($result);
    }


    public function isEmailRegistered(Request $request)
    {
        $result = array();
        $email = $request->post('email');
        $isEmailRegistered = User::getUserByEmail($email);
        if (!empty($isEmailRegistered)) {
            $result['success'] = true;
            $result['msg'] = "Email already registered.";
        }
        return json_encode($result);
    }

    public function register(Request $request)
    {
        $result = array();
        $currentUrl = URL::current();
        $email = $request->post('email_id');
        $phone = (int) $request->post('phone');
        $isEmailRegistered = User::getUserByEmailAndPhone($email, $phone);
        if (empty($isEmailRegistered)) {
            $user = new User();
            $data = array();
            foreach ($request->all() as $key => $value) {
                if (!in_array($key, ['_token', 'password2'])) {
                    $data[$key] = $value;
                    if ($key == 'password') {
                        $value = sha1($value);
                    }
                    $user->$key = $value;
                }
            }
            if ($user->save()) {
                $data['user_id'] = $user->id; //Adding User Id
                $data['user_type'] = 'Staff'; //Adding User type

                // Remove special characters
                $data = removeSpecialCharacters($data);

                //Creating the CSV File
                $columnsNames = ['CustomerName', 'CustomerNumber', 'EmailID', 'Location', 'SecondaryName',
                    'SecondaryNumber', 'SecondaryEmail', 'Password', 'UserId', 'UserType'];
                $fileName = 'NewUserFromPortal_' . date('His') . '.csv';
                $csv = createCSV($columnsNames, $data, env('CUST_REGISTRATION_CSV_CREATION_PATH') . $fileName);

                /*$emailData = array('name' => $request->get('name'));
                Mail::send('emails.registration', $emailData, function ($message) use ($request) {
                    $message->to($request->get('email_id'), $request->get('name'))->subject
                    ('Registration Successful on Inaya Portal');
                    $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                });*/

                createLog('newuserregistration@inaya.ae', $currentUrl, 'CSV Created', '', $fileName);

                //Starting the SFTP Transfer
                $sftp = new SFTP(env('SFTP_HOST'));
                $sftp_login = $sftp->login(env('SFTP_USERNAME'), env('SFTP_PASSWORD'));

                if ($sftp_login) {
                    createLog('newuserregistration@inaya.ae', $currentUrl, 'SFTP Logged In Success', $sftp, $sftp_login);
                    $remoteFile = env('CUST_REGISTRATION_CSV_UPLOAD_PATH') . env('DEFAULT_CUSTOMER_REGISTER_FILE_NAME');
                    $localFile = env('CUST_REGISTRATION_CSV_CREATION_PATH') . $fileName;
                    $transfer = $sftp->put($remoteFile, $localFile, SFTP::SOURCE_LOCAL_FILE);
                    if ($transfer) {
                        createLog('newuserregistration@inaya.ae', $currentUrl, 'SFTP Transfer Success', ['remoteFile' => $remoteFile, 'localFile' => $localFile], $transfer);
                        $result['success'] = true;
                        $result['msg'] = "Registration successful, please wait we are sending OTP on your mobile phone.";
                        $request->session()->put('user_register', $user);
                    } else {
                        $error = $sftp->getLastSFTPError();
                        $result['success'] = false;
                        $result['msg'] = getSFTPError($error);
                        createLog('newuserregistration@inaya.ae', $currentUrl, 'SFTP Transfer Failed', ['remoteFile' => $remoteFile, 'localFile' => $localFile], $error);
                    }
                    $sftp->disconnect();
                } else {
                    $error = $sftp->getLastSFTPError();
                    $result['success'] = false;
                    $result['msg'] = getSFTPError($error);
                    createLog('newuserregistration@inaya.ae', $currentUrl, 'SFTP Login Failed', $sftp, $sftp_login);
                }
            } else {
                $result['success'] = false;
                $result['msg'] = "Error in saving User.";
            }
        } else {
            $result['success'] = false;
            $result['msg'] = "User Already Registered.";
        }
        return json_encode($result);
    }

    public function viewVerifyOTP(Request $request)
    {
        if ($request->session()->has('user_details')) {
            return redirect('/');
        }
        if ($request->session()->has('user_register')) {
            $registeredDetails = $request->session()->get('user_register');
            $otp = rand(111111, 999999);
            $request->session()->put('otp', $otp);
            $sms = "Your Inaya Portal OTP Code is:" . $otp;
            sendSMS($registeredDetails->phone, $sms);
            return view('verify-otp', ['phone' => $registeredDetails->phone]);
        } else {
            return redirect('/');
        }
    }

    public function resendOTP(Request $request)
    {
        $result = array();
        if ($request->session()->has('user_register') && $request->session()->has('otp')) {
            $registeredDetails = $request->session()->get('user_register');
            $sms = "Your Inaya Portal OTP Code is:" . $request->session()->get('otp');
            sendSMS($registeredDetails->phone, $sms);
            $result['success'] = true;
            $result['msg'] = "OTP Sent.";
        } else {
            $result['success'] = false;
            $result['msg'] = "Invalid Request";
        }
        return json_encode($result);
    }

    public function verifyOTP(Request $request)
    {
        $result = array();
        if ($request->session()->has('user_register') && $request->session()->has('otp')) {
            $otp = $request->post('otp');
            if ($request->session()->get('otp') == $otp) {
                $user = $request->session()->get('user_register');
                $user->mobile_verify_status = 'Y';
                if ($user->save()) {
                    $request->session()->put('user_details', $request->session()->get('user_register'));
                    $request->session()->pull('otp');
                    $request->session()->pull('user_register');
                    $result['success'] = true;
                    $result['msg'] = 'OTP Verified';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'Invalid OTP Entered';
            }
        } else {
            $result['success'] = false;
            $result['msg'] = 'Invalid Request';
        }
        return json_encode($result);
    }

    public function updateProfile(Request $request)
    {
        $result = array();
        $currentUrl = URL::current();
        $userId = $request->session()->get('user_details')->id;
        if (!empty($userId)) {
            $user = User::where('id', $userId)->first();
            if (!empty($user)) {
                foreach ($request->all() as $key => $value) {
                    if (!in_array($key, ['_token'])) {
                        $user->$key = $value;
                    }
                }
                if ($user->save()) {
                    $request->session()->put('user_details', $user);
                    $data = $user->attributesToArray();
                    unset($data['id']);
                    unset($data['password']);
                    unset($data['is_admin']);
                    unset($data['mobile_verify_status']);
                    unset($data['created_at']);
                    unset($data['updated_at']);
                    $data['user_id'] = $user->id; //Adding User Id
                    $data['user_type'] = 'Staff'; //Adding User Type

                    // Remove special characters
                    $data = removeSpecialCharacters($data);

                    //Creating the CSV File
                    $columnsNames = ['CustomerName', 'CustomerNumber', 'EmailID', 'Location', 'SecondaryName',
                        'SecondaryNumber', 'SecondaryEmail', 'UserId', 'UserType'];
                    $fileName = 'UserProfileUpdateFromPortal_' . date('His') . '.csv';
                    $csv = createCSV($columnsNames, $data, env('CUST_PROFILE_UPDATE_CSV_CREATION_PATH') . $fileName);

                    /*$emailData = array('name' => $request->get('name'));
                    Mail::send('emails.registration', $emailData, function ($message) use ($request) {
                        $message->to($request->get('email_id'), $request->get('name'))->subject
                        ('Registration Successful on Inaya Portal');
                        $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                    });*/

                    createLog('userprofileupdate@inaya.ae', $currentUrl, 'CSV Created', '', $fileName);

                    //Starting the SFTP Transfer
                    $sftp = new SFTP(env('SFTP_HOST'));
                    $sftp_login = $sftp->login(env('SFTP_USERNAME'), env('SFTP_PASSWORD'));

                    if ($sftp_login) {
                        createLog('userprofileupdate@inaya.ae', $currentUrl, 'SFTP Logged In Success', $sftp, $sftp_login);
                        $remoteFile = env('CUST_PROFILE_UPDATE_CSV_UPLOAD_PATH') . env('DEFAULT_CUSTOMER_PROFILE_UPDATE_FILE_NAME');
                        $localFile = env('CUST_PROFILE_UPDATE_CSV_CREATION_PATH') . $fileName;
                        $transfer = $sftp->put($remoteFile, $localFile, SFTP::SOURCE_LOCAL_FILE);
                        if ($transfer) {
                            createLog('userprofileupdate@inaya.ae', $currentUrl, 'SFTP Transfer Success', ['remoteFile' => $remoteFile, 'localFile' => $localFile], $transfer);
                            $result['result'] = 'success';
                        } else {
                            $error = $sftp->getLastSFTPError();
                            $result['result'] = 'failed';
                            $result['message'] = getSFTPError($error);
                            createLog('userprofileupdate@inaya.ae', $currentUrl, 'SFTP Transfer Failed', ['remoteFile' => $remoteFile, 'localFile' => $localFile], $error);
                        }
                        $sftp->disconnect();
                    } else {
                        $error = $sftp->getLastSFTPError();
                        $result['result'] = 'failed';
                        $result['message'] = getSFTPError($error);
                        createLog('userprofileupdate@inaya.ae', $currentUrl, 'SFTP Login Failed', $sftp, $sftp_login);
                    }

                    $result['success'] = true;
                    $result['msg'] = "Profile updated successfully";
                } else {
                    $result['success'] = false;
                    $result['msg'] = "Invalid Request, Please logout and login again.";
                }
            } else {
                $result['success'] = false;
                $result['msg'] = "Invalid Request, Please logout and login again.";
            }
            return json_encode($result);
        }
    }

    public function changePassword(Request $request)
    {
        $result = array();
        $userId = $request->session()->get('user_details')->id;
        if (!empty($userId)) {
            $user = User::where('id', $userId)->first();
            if (!empty($user)) {
                if (sha1($request->get('password')) == $user->password) {
                    if ($request->get('new_password') == $request->get('confirm_password') &&
                        !empty($request->get('new_password'))) {
                        $user->password = sha1($request->get('new_password'));
                        if ($user->save()) {
                            $result['success'] = true;
                            $result['msg'] = "Password changed successfully.";
                        } else {
                            $result['success'] = false;
                            $result['msg'] = "Some error occurred while changing the password.";
                        }
                    } else {
                        $result['success'] = false;
                        $result['msg'] = "New Passwords not matched.";
                    }
                } else {
                    $result['success'] = false;
                    $result['msg'] = "Current Password is Invalid";
                }
            } else {
                $result['success'] = false;
                $result['msg'] = "Invalid Request, Please logout and login again.";
            }
            return json_encode($result);
        }
    }
}
