<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use App\Models\NatureComplaintModel;
use App\Models\UnitMasterModel;
use App\Models\UserRequest;
use App\Models\UserUpdateRequestModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use phpseclib\Net\SFTP;

class RequestController extends Controller
{
    public function newRequest(Request $request)
    {
        $unitIds = array();
        $unitIds[] = 'INPX-XXX-XXX-XXX-XXX-XXXX-C';
        $userDetails = $request->session()->get('user_details');
        $units = UnitMasterModel::select('unit_id')
            ->where('contact_phone', $userDetails->phone)->distinct()->get();
        if (!empty($units) && count($units)) {
            foreach ($units as $unit) {
                if ($unit['unit_id'] != 'INPX-XXX-XXX-XXX-XXX-XXXX-C') {
                    $unitIds[] = $unit['unit_id'];
                }
            }
        }
        $natureOfComplaints = NatureComplaintModel::select('id', 'name')->where('status', 'Y')->get();
        $categories = CategoryModel::where('parent_id', 0)->where('status', 'Y')->get();
        return view('new_request',
            ['userDetails' => $userDetails, 'unitIds' => $unitIds,
                'categories' => $categories, 'natureOfComplaints' => $natureOfComplaints]);
    }

    public function submitRequest(Request $request)
    {
        $result = array();
        $currentUrl = URL::current();
        $userDetails = $request->session()->get('user_details');

        $natureOfComplaint = NatureComplaintModel::where('id', $request->nature_of_complaint)->first();
        $initial = $natureOfComplaint->initials;

        createLog('complaintsubmit@inaya.ae', $currentUrl, 'Request Submitted', $request->all(), '');

        $data = [];
        $data['client_sr_no'] = '';
        $data['processing_remarks'] = $request->description;
        $data['unit_id'] = $request->unit_id;
        $data['created_at'] = date('d-m-Y H:i:s');
        $data['priority'] = $request->priority;
        $data['problem_code'] = 'OTH';
        $data['category'] = $request->work_order_category;
        $data['swo_type'] = $request->failure_class;
        $data['nature_of_complaint'] = $natureOfComplaint->name;
        $data['cust_primary_name'] = $userDetails->name;
        $data['cust_primary_phone'] = $userDetails->phone;
        $data['cust_primary_email'] = $userDetails->email_id;
        $data['cust_secondary_name'] = $request->secondary_contact_name;
        $data['cust_secondary_phone'] = $request->secondary_contact_phone;
        $data['cust_secondary_email'] = $request->secondary_contact_email;
        $data['assigned_to'] = $initial;
        $data['status'] = 'Y';
        $data['user_id'] = $userDetails->id;
        $data['user_type'] = Config::get('constants.CUSTOMER_USER_TYPE');

        $userRequest = new UserRequest();
        foreach ($data as $key => $val) {
            $userRequest->$key = $val;
        }

        $data['cust_primary_email'] = $userDetails->name; //Changed as per CSV Format
        unset($data['description']); //Removed as per CSV Format
        unset($data['user_id']); //Removed as per CSV Format
        unset($data['user_type']); //Removed as per CSV Format

        // Checking if Data is stored
        if ($userRequest->save()) {
            $userRequest->client_sr_no = $initial . str_pad($userRequest->id, 7, 0, STR_PAD_LEFT);
            $data['client_sr_no'] = $userRequest->client_sr_no;
            $userRequest->save();
            createLog('complaintsubmit@inaya.ae', $currentUrl, 'Data stored in Database', $data, $userRequest->id);

            // Remove special characters
            $data = removeSpecialCharacters($data);

            //Creating the CSV File
            $columnsNames = ['Client SR No', 'Processing Remarks', 'Unit', 'Client SR Date and Time', 'Priority',
                'Problem Code', 'Work Order Category', 'Failure Class', 'Nature of Complaint',
                'Customer Primary Name (Based on the Unit Mastr)', 'Contact Phone no.', 'Customer Name',
                'Call Originator', 'Originators Phone No.', 'customer email', 'Assigned to', 'status_flag'];
            $fileName = 'ComplaintFromPortal_' . date('His') . '.csv';
            $csv = createCSV($columnsNames, $data, env('CUST_REQUEST_CSV_CREATION_PATH') . $fileName);

            createLog('complaintsubmit@inaya.ae', $currentUrl, 'CSV Created', '', $fileName);

            //Starting the SFTP Transfer
            $sftp = new SFTP(env('SFTP_HOST'));
            $sftp_login = $sftp->login(env('SFTP_USERNAME'), env('SFTP_PASSWORD'));

            if ($sftp_login) {
                createLog('complaintsubmit@inaya.ae', $currentUrl, 'SFTP Logged In Success', $sftp, $sftp_login);
                $remoteFile = env('CUST_REQUEST_CSV_UPLOAD_PATH') . env('DEFAULT_CUSTOMER_FILE_NAME');
                $localFile = env('CUST_REQUEST_CSV_CREATION_PATH') . $fileName;
                $transfer = $sftp->put($remoteFile, $localFile, SFTP::SOURCE_LOCAL_FILE);
                if ($transfer) {
                    createLog('complaintsubmit@inaya.ae', $currentUrl, 'SFTP Transfer Success', ['remoteFile' => $remoteFile, 'localFile' => $localFile], $transfer);
                    $result['result'] = 'success';
                } else {
                    $error = $sftp->getLastSFTPError();
                    $result['result'] = 'failed';
                    $result['message'] = getSFTPError($error);
                    createLog('complaintsubmit@inaya.ae', $currentUrl, 'SFTP Transfer Failed', ['remoteFile' => $remoteFile, 'localFile' => $localFile], $error);
                }
                $sftp->disconnect();
            } else {
                $error = $sftp->getLastSFTPError();
                $result['result'] = 'failed';
                $result['message'] = getSFTPError($error);
                createLog('complaintsubmit@inaya.ae', $currentUrl, 'SFTP Login Failed', $sftp, $sftp_login);
            }
        } else {
            $result['result'] = 'failed';
            $result['message'] = 'Something went wrong while saving the Data';
            createLog('complaintsubmit@inaya.ae', $currentUrl, 'Error in Storing into Database', $data, '');
        }
        return $result;
    }

    public function listPlacedRequest(Request $request)
    {
        $userDetails = $request->session()->get('user_details');
        if ($userDetails->is_admin == 'Y') {
            $placedRequests = UserRequest::orderBy('created_at', 'DESC')->get();
        } else {
            $placedRequests = UserRequest::where('user_id', $userDetails->id)
                ->where('user_type', Config::get('constants.CUSTOMER_USER_TYPE'))
                ->orderBy('created_at', 'DESC')
                ->get();
        }
        return view('list_placed_request', ['placedRequests' => $placedRequests]);
    }

    public function listUpdateRequest(Request $request)
    {
        $userDetails = $request->session()->get('user_details');
        if ($userDetails->is_admin == 'Y') {
            $statusTypes = UserUpdateRequestModel::select('status')
                ->distinct()
                ->get();
        } else {
            $statusTypes = UserUpdateRequestModel::select('status')
                ->where('customer_phone', $userDetails->phone)
                ->where('status', '!=', '')
                ->distinct()
                ->get();
        }
        return view('list_update_request', ['statusTypes' => $statusTypes]);
    }

    public function getUpdateRequest(Request $request)
    {
        $userDetails = session()->get('user_details');
        // Read value
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        $statusFilter = $request->get('statusFilter');

        // Total records
        if ($userDetails->is_admin == 'Y') {
            $totalRecords = UserUpdateRequestModel::select('count(*) as allcount')->count();
        } else {
            $totalRecords = UserUpdateRequestModel::select('count(*) as allcount')->where('customer_phone', $userDetails->phone)->count();
        }

        $totalRecordswithFilterQuery = DB::table('user_update_requests');
        $totalRecordswithFilterQuery->select('id');

        $totalRecordswithFilterQuery = $this->getUserUpdateWhereCondition($totalRecordswithFilterQuery, $searchValue, $statusFilter);

        $totalRecordswithFilter = $totalRecordswithFilterQuery->get()->count();

        //Fetch records
        $updateRequestsQuery = DB::table('user_update_requests');
        $updateRequestsQuery->select('complaint_no', 'swo_type', 'swo_description', 'unit_id', 'customer_name',
        'customer_phone', 'secondary_name', 'secondary_phone_no', 'category', 'status', 'created_date', 'created_time');
        $updateRequestsQuery = $this->getUserUpdateWhereCondition($updateRequestsQuery, $searchValue, $statusFilter);

        $updateRequestsQuery->orderBy($columnName, $columnSortOrder);
        $updateRequestsQuery->skip($start);
        $updateRequestsQuery->take($rowperpage);
        $updateRequests = $updateRequestsQuery->get();
        /*// Fetch records
        $records = Employees::orderBy($columnName, $columnSortOrder)
            ->where('employees.name', 'like', '%' . $searchValue . '%')
            ->select('employees.*')
            ->skip($start)
            ->take($rowperpage)
            ->get();*/

        $data_arr = array();
        $sno = $start + 1;
        foreach ($updateRequests as $request) {
            $data_arr[] = [
                "complaint_no" => $request->complaint_no,
                "customer_name" => $request->customer_name,
                "customer_phone" => !empty($request->customer_phone) ? "+971 ". ltrim($request->customer_phone, "0") : "",
                "swo_type" => $request->swo_type,
                "swo_description" => $request->swo_description,
                "unit_id" => $request->unit_id,
                "secondary_name" => $request->secondary_name,
                "secondary_phone_no" => !empty($request->secondary_phone_no) ? "+971 ". ltrim($request->secondary_phone_no, "0") : "",
                "category" => $request->category,
                "status" => $request->status,
                "created_date" => $request->created_date,
                "created_time" => $request->created_time
            ];
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        );

        return json_encode($response);
    }

    public function getUserUpdateWhereCondition($query, $searchValue, $statusFilter)
    {
        if (!empty($searchValue)) {
            $query->where('complaint_no', 'like', '%' . $searchValue . '%');
            $query->orWhere('swo_type', 'like', '%' . $searchValue . '%');
            $query->orWhere('swo_description', 'like', '%' . $searchValue . '%');
            $query->orWhere('unit_id', 'like', '%' . $searchValue . '%');
            $query->orWhere('customer_name', 'like', '%' . $searchValue . '%');
            $query->orWhere('customer_phone', 'like', '%' . $searchValue . '%');
            $query->orWhere('secondary_name', 'like', '%' . $searchValue . '%');
            $query->orWhere('secondary_phone_no', 'like', '%' . $searchValue . '%');
            $query->orWhere('category', 'like', '%' . $searchValue . '%');
            $query->orWhere('status', 'like', '%' . $searchValue . '%');
            $query->orWhere('created_date', 'like', '%' . $searchValue . '%');
            $query->orWhere('created_time', 'like', '%' . $searchValue . '%');;
        }
        if (!empty($statusFilter)) {
            $query->where('status', trim($statusFilter));
        }
        $userDetails = session()->get('user_details');
        if($userDetails->is_admin != 'Y') {
            $query->where('customer_phone', $userDetails->phone);
        }
        return $query;
    }
}
