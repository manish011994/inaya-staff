<?php

namespace App\Http\Controllers;

use App\Models\CustomerUser;
use App\Models\PhoneUpdateRequestModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PhoneChangeRequestController extends Controller
{
    public function managePhoneChangeRequest(Request $request)
    {
        $requests = PhoneUpdateRequestModel::where('status', 'Pending')->get();
        return view('phone_change_request.manage_phone_change_request', ['requests' => $requests]);
    }

    public function approveRequest(Request $request, $requestId)
    {
        $result = array();
        $request = PhoneUpdateRequestModel::where('id', $requestId)->where('status', 'Pending')->first();
        if (!empty($request)) {
            $userDetails = CustomerUser::getUser($request->user_id);
            $userDetails->phone = $request->new_phone;
            if ($userDetails->save()) {
                $request->status = 'Accept';
                $request->save();
                $sms = "Dear ".$userDetails->name.",\n
                        Your phone number change request is completed.\n
                        Phone number is changed to ".$request->new_phone.".";
                sendSMS($request->new_phone, $sms);
                sendSMS($request->secondary_phone, $sms);
                $emailData = array('phoneUpdateRequest' => $request);
                Mail::send('emails.user_phone_change_request', $emailData, function ($message) use ($userDetails) {
                    $message->to($userDetails->email_id, $userDetails->name)
                        ->bcc($userDetails->secondary_email, $userDetails->secondary_name)
                        ->subject('Your Phone change request completed on Inaya Portal');
                    $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                });
                $result['result'] = 'success';
                $result['msg'] = "Phone no updated.";
            } else {
                $result['result'] = 'failed';
                $result['msg'] = "Error in updating.";
            }
            return json_encode($result);
        } else {
            return abort(404);
        }
    }


    public function rejectRequest(Request $request, $requestId)
    {
        $result = array();
        $request = PhoneUpdateRequestModel::where('id', $requestId)->where('status', 'Pending')->first();
        if (!empty($request)) {
            $request->status = 'Reject';
            if ($request->save()) {
                $result['result'] = "success";
                $result['msg'] = "Phone no update request rejected.";
            } else {
                $result['result'] = "failed";
                $result['msg'] = "Error in rejecting.";
            }
            return json_encode($result);
        } else {
            return abort(404);
        }
    }
}
