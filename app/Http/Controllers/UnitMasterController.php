<?php

namespace App\Http\Controllers;

use App\Models\UnitMasterFilesMetaDataModel;
use App\Models\UserRequestsFilesMetaDataModel;
use Illuminate\Support\Facades\URL;
use phpseclib\Net\SFTP;

class UnitMasterController extends Controller
{
    public function getUnitIds()
    {
        ini_set('max_execution_time', 0);
        //Starting the SFTP Transfer
        $fileLists = array();
        $sftp = new SFTP(env('SFTP_HOST'));
        $currentUrl = URL::current();
        $sftp_login = $sftp->login(env('SFTP_USERNAME'), env('SFTP_PASSWORD'));

        if ($sftp_login) {
            createLog('unitmastercron@inaya.ae', $currentUrl, 'SFTP Logged In Success', $sftp, $sftp_login);
            $sftp->chdir(env('CUST_UNIT_MASTER_PATH'));
            $files = $sftp->rawlist();
            r_print("Files Loaded");
            foreach ($files as $file) {
                if (!in_array($file['filename'], ['.', '..']) &&
                    strtolower(pathinfo($file['filename'])['extension']) == 'csv') { //Exclude These from the File list
                    $unitModelsMetaData = UnitMasterFilesMetaDataModel::getFileDetails($file);
                    if ($unitModelsMetaData == 'download') {
                        $fileLists[] = $file;
                        //File Not Present
                        // Add the File in MetaData Table
                        $unitMetaData = new UnitMasterFilesMetaDataModel();
                        $unitMetaData->file_name = $file['filename'];
                        $unitMetaData->file_size = $file['size'];
                        $unitMetaData->file_created = $file['atime'];
                        $unitMetaData->last_modified = $file['mtime'];
                        if ($unitMetaData->save()) {
                            r_print("Meta Data Saved for - " . $unitMetaData->file_name);
                            $sftp->ping();
                            $storeFile = $sftp->get(env('CUST_UNIT_MASTER_PATH') . $file['filename'], env('DOWNLOAD_UNIT_MASTER_PATH') . $file['filename']);
                            $sftp->ping();
                        } else {
                            createLog('unitmastercron@inaya.ae', $currentUrl, 'File Meta Data Saving Failed for File - ' . $file['filename'], $unitMetaData, "Error in saving");
                            // TODO Send Email
                        }
                    } else if ($unitModelsMetaData == 'update_download') {
                        //File Present
                        // Update the File in MetaData Table
                        $unitMetaData = UnitMasterFilesMetaDataModel::where('file_created', $file['atime'])->first();
                        $unitMetaData->file_name = $file['filename'];
                        $unitMetaData->file_size = $file['size'];
                        $unitMetaData->file_created = $file['atime'];
                        $unitMetaData->last_modified = $file['mtime'];
                        if ($unitMetaData->save()) {
                            r_print("Meta Data Updated for - " . $unitMetaData->file_name);
                            $sftp->ping();
                            $storeFile = $sftp->get(env('CUST_UNIT_MASTER_PATH') . $file['filename'], env('DOWNLOAD_UNIT_MASTER_PATH') . $file['filename']);
                            $sftp->ping();
                        } else {
                            createLog('unitmastercron@inaya.ae', $currentUrl, 'File Meta Data Saving Failed for File - ' . $file['filename'], $unitMetaData, "Error in saving");
                            // TODO Send Email
                        }
                    }
                }
            }
            $sftp->disconnect();
            //Store Data in Unit Master Table
            storeUnitMasterDataFromCSV($fileLists);
            r_print("Process Finished");
        } else {
            $error = $sftp->getLastSFTPError();
            $result['result'] = 'failed';
            $result['message'] = getSFTPError($error);
            createLog('unitmastercron@inaya.ae', $currentUrl, 'SFTP Login Failed', $sftp, $sftp_login);
        }
    }

    public function getCustUpdateRequest()
    {
        ini_set('max_execution_time', 0);
        //Starting the SFTP Transfer
        $fileLists = array();
        $sftp = new SFTP(env('SFTP_HOST'));
        $currentUrl = URL::current();
        $sftp_login = $sftp->login(env('SFTP_USERNAME'), env('SFTP_PASSWORD'));

        if ($sftp_login) {
            createLog('custupdaterequestcron@inaya.ae', $currentUrl, 'SFTP Logged In Success', $sftp, $sftp_login);
            $sftp->chdir(env('CUST_CALL_UPDATE_REQUEST'));
            $files = $sftp->rawlist();
            r_print("Files Loaded");
            foreach ($files as $file) {
                if (!in_array($file['filename'], ['.', '..']) &&
                    strtolower(pathinfo($file['filename'])['extension']) == 'csv') { //Exclude These from the File list
                    $userUpdateRequestMetaModel = UserRequestsFilesMetaDataModel::getFileDetails($file);
                    $fileLists[] = $file;
                    if ($userUpdateRequestMetaModel == 'download') {
                        //File Not Present
                        // Add the File in MetaData Table
                        $userUpdateRequestData = new UserRequestsFilesMetaDataModel();
                        $userUpdateRequestData->file_name = $file['filename'];
                        $userUpdateRequestData->file_size = $file['size'];
                        $userUpdateRequestData->file_created = $file['atime'];
                        $userUpdateRequestData->last_modified = $file['mtime'];
                        if ($userUpdateRequestData->save()) {
                            r_print("Meta Data Saved for - " . $userUpdateRequestData->file_name);
                            $sftp->ping();
                            $storeFile = $sftp->get(env('CUST_CALL_UPDATE_REQUEST') . $file['filename'], env('DOWNLOAD_CUST_UPDATE_REQUEST_PATH') . $file['filename']);
                            $sftp->ping();
                        } else {
                            createLog('custupdaterequestcron@inaya.ae', $currentUrl, 'File Meta Data Saving Failed for File - ' . $file['filename'], $userUpdateRequestData, "Error in saving");
                            // TODO Send Email
                        }
                    } else if ($userUpdateRequestMetaModel == 'update_download') {
                        //File Present
                        // Update the File in MetaData Table
                        $userUpdateRequestData = UserRequestsFilesMetaDataModel::where('file_created', $file['atime'])->first();
                        $userUpdateRequestData->file_name = $file['filename'];
                        $userUpdateRequestData->file_size = $file['size'];
                        $userUpdateRequestData->file_created = $file['atime'];
                        $userUpdateRequestData->last_modified = $file['mtime'];
                        if ($userUpdateRequestData->save()) {
                            r_print("Meta Data Updated for - " . $userUpdateRequestData->file_name);
                            $sftp->ping();
                            $storeFile = $sftp->get(env('CUST_CALL_UPDATE_REQUEST') . $file['filename'], env('DOWNLOAD_CUST_UPDATE_REQUEST_PATH') . $file['filename']);
                            $sftp->ping();
                        } else {
                            createLog('custupdaterequestcron@inaya.ae', $currentUrl, 'File Meta Data Saving Failed for File - ' . $file['filename'], $userUpdateRequestData, "Error in saving");
                            // TODO Send Email
                        }
                    }
                }
            }
            $sftp->disconnect();
            //Store Data in Unit Master Table
            storeUserUpdateRequestDataFromCSV($fileLists);
            r_print("Process Finished");
        } else {
            $error = $sftp->getLastSFTPError();
            $result['result'] = 'failed';
            $result['message'] = getSFTPError($error);
            createLog('custupdaterequestcron@inaya.ae', $currentUrl, 'SFTP Login Failed', $sftp, $sftp_login);
        }
    }
}
