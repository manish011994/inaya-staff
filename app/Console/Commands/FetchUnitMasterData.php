<?php

namespace App\Console\Commands;

use App\Models\UnitMasterFilesMetaDataModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;
use phpseclib\Net\SFTP;

class FetchUnitMasterData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unit_master';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To fetch the Unit Ids from Ramco Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        //Starting the SFTP Transfer
        $fileLists = array();
        $sftp = new SFTP(env('SFTP_HOST'));
        $currentUrl = URL::current();
        $sftp_login = $sftp->login(env('SFTP_USERNAME'), env('SFTP_PASSWORD'));

        if ($sftp_login) {
            createLog('unitmastercron@inaya.ae', $currentUrl, 'SFTP Logged In Success', $sftp, $sftp_login);
            $sftp->chdir(env('CUST_UNIT_MASTER_PATH'));
            $files = $sftp->rawlist();
            $this->info("Files Loaded");
            foreach ($files as $file) {
                if (!in_array($file['filename'], ['.', '..']) &&
                    strtolower(pathinfo($file['filename'])['extension']) == 'csv') { //Exclude These from the File list
                    $unitModelsMetaData = UnitMasterFilesMetaDataModel::getFileDetails($file);
                    if ($unitModelsMetaData == 'download') {
                        $fileLists[] = $file;
                        //File Not Present
                        // Add the File in MetaData Table
                        $unitMetaData = new UnitMasterFilesMetaDataModel();
                        $unitMetaData->file_name = $file['filename'];
                        $unitMetaData->file_size = $file['size'];
                        $unitMetaData->file_created = $file['atime'];
                        $unitMetaData->last_modified = $file['mtime'];
                        if ($unitMetaData->save()) {
                            $this->info("Meta Data Saved for - " . $unitMetaData->file_name);
                            $sftp->ping();
//                            $storeFile = $sftp->get(env('CUST_UNIT_MASTER_PATH') . $file['filename'], env('DOWNLOAD_UNIT_MASTER_PATH') . $file['filename']);
                            $size = $sftp->size(env('CUST_UNIT_MASTER_PATH') . $file['filename']);
                            for ($offset = 0; $offset < $size; $offset += 1024 * 1024) {
                                $sftp->get(env('CUST_UNIT_MASTER_PATH') . $file['filename'], env('DOWNLOAD_UNIT_MASTER_PATH') . $file['filename'], $offset, 1024 * 1024);
                            }
                            $sftp->ping();
                        } else {
                            createLog('unitmastercron@inaya.ae', $currentUrl, 'File Meta Data Saving Failed for File - ' . $file['filename'], $unitMetaData, "Error in saving");
                            // TODO Send Email
                        }
                    } else if ($unitModelsMetaData == 'update_download') {
                        //File Present
                        // Update the File in MetaData Table
                        $unitMetaData = UnitMasterFilesMetaDataModel::where('file_created', $file['atime'])->first();
                        $unitMetaData->file_name = $file['filename'];
                        $unitMetaData->file_size = $file['size'];
                        $unitMetaData->file_created = $file['atime'];
                        $unitMetaData->last_modified = $file['mtime'];
                        if ($unitMetaData->save()) {
                            $this->info("Meta Data Updated for - " . $unitMetaData->file_name);
                            $sftp->ping();
//                            $storeFile = $sftp->get(env('CUST_UNIT_MASTER_PATH') . $file['filename'], env('DOWNLOAD_UNIT_MASTER_PATH') . $file['filename']);
                            $size = $sftp->size(env('CUST_UNIT_MASTER_PATH') . $file['filename']);
                            for ($offset = 0; $offset < $size; $offset += 1024 * 1024) {
                                $sftp->get(env('CUST_UNIT_MASTER_PATH') . $file['filename'], env('DOWNLOAD_UNIT_MASTER_PATH') . $file['filename'], $offset, 1024 * 1024);
                            }
                            $sftp->ping();
                        } else {
                            createLog('unitmastercron@inaya.ae', $currentUrl, 'File Meta Data Saving Failed for File - ' . $file['filename'], $unitMetaData, "Error in saving");
                            // TODO Send Email
                        }
                    }
                }
            }
            $sftp->disconnect();
            //Store Data in Unit Master Table
            storeUnitMasterDataFromCSV($fileLists, $this);
            $this->info('Process Finished.');
        } else {
            $error = $sftp->getLastSFTPError();
            $result['result'] = 'failed';
            $result['message'] = getSFTPError($error);
            createLog('unitmastercron@inaya.ae', $currentUrl, 'SFTP Login Failed', $sftp, $sftp_login);
        }
    }
}
