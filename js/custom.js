"use strict";

$(function () {

    $("#work_order_category").change(function () {
        console.log($(this).data('token'));
        var val = $(this).val();
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: '/get-category', // the url where we want to POST
            data: {"category": val, "_token": $(this).data('token')}, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            beforeSend: function () {
                $("#failure_class").html("<option value=''>Fetching, Please wait</option>");
            },
        }).done(function (data) {
            $("#failure_class").html("<option value=''>Please select failure class</option>");
            $.each(data, function (key, value) {
                $("#failure_class").append("<option value='" + value.category + "'>" + value.category + "</option>");
            });
        }).fail(function () {
            alert("Some Error Occurred on the Server.");
        });
    });

    $("#new_request").submit(function (e) {
        let $form = $(this);
        let action = $form.attr('action');
        let formData = $form.serialize();

        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: action, // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            beforeSend: function () {
                $("#response_msg").removeClass("alert-success").removeClass("alert-danger").html("");
                $("#register").html("<i class=\"fa fa-spinner fa-spin\"></i> &nbsp; Please wait");
            },
        })
            .done(function (data) {
                if (data.result == "success") {
                    $form[0].reset();
                    $("#response_msg").addClass("alert-success").html("Your request placed successfully.");
                } else if (data.result == "failed") {
                    $("#response_msg").addClass("alert-danger").html(data.message);
                } else {
                    $("#response_msg").addClass("alert-danger").html("Something went wrong the submitted request.");
                }
            })
            .fail(function () {
                $("#response_msg").addClass("alert-danger").html("Some Error Occurred on the Server.");
            })
            .always(function () {
                $("#register").html("Submit");
            });
        e.preventDefault();
    });

    $("#job_status").change(function () {
        console.log($(this).val());
        if ($(this).val() != "") {
            $(".data-row").hide();
            $("." + $(this).val().replace(" ", "_")).show();
        } else {
            $(".data-row").show();
        }
    });
});
