@extends('app')
@section('title') Inaya Portal - List Placed Request @endsection
@section('css_content')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
@endsection

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Staff - List Placed Request</h1>
            </div>

            <div class="section-body">

                <div class="card">
                    <div class="card-body">
                        {{--<div class="row">
                            <div class="col-12 text-right">
                                <label for="job_status">Filter By:</label>
                                <select class="" name="job_status" id="job_status">
                                    <option value="">Please select Job Type</option>
                                    <option value="Fresh Appointment">Fresh Appointment</option>
                                    <option value="Awaiting Acknowledgement">Awaiting Acknowledgement</option>
                                    <option value="Appointment Rescheduled">Appointment Rescheduled</option>
                                    <option value="Job Completed">Job Completed</option>
                                </select>
                            </div>
                        </div>
                        <hr/>--}}
                        <div class="table-responsive">
                            <table id="list_request" width="100%"
                                   class="table table-sm table-striped table-hover font-size-12">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Sr No</th>
                                    <th>Priority</th>
                                    <th>SWO</th>
                                    <th>Description</th>
                                    <th>S. Name</th>
                                    <th>S. Phone</th>
                                    <th>S. Email</th>
                                    <th>Unit ID</th>
                                    <th>Category</th>
                                    <th>Created DateTime</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($placedRequests) && count($placedRequests))
                                    @foreach($placedRequests as $request)
                                        <tr style="font-size: 11px;">
                                            <td>{{ $request->cust_primary_name }}</td>
                                            <td>+971 {{ $request->cust_primary_phone }}</td>
                                            <td>{{ $request->cust_primary_email }}</td>
                                            <td>{{ $request->client_sr_no }}</td>
                                            <td>{{ $request->priority }}</td>
                                            <td>{{ $request->nature_of_complaint }}</td>
                                            <td>{{ $request->processing_remarks }}</td>
                                            <td>{{ $request->cust_secondary_name }}</td>
                                            <td>+971 {{ $request->cust_secondary_phone }}</td>
                                            <td>{{ $request->cust_secondary_email }}</td>
                                            <td>{{ $request->unit_id }}</td>
                                            <td>{{ $request->category }}</td>
                                            <td>{{ $request->created_at }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('js_content')
    <script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $("#list_request").DataTable({
                "pageLength": 50,
                "scrollY": "400px",
                "scrollCollapse": true,
                "order": [12, 'desc'],
                "dom": '<"top"lpf>rt<"bottom"ip><"clear">',
                "lengthMenu": [[50, 100, 250, 500, 1000, "All"], [50, 100, 250, 500, 1000, "All"]]
            });
        });
    </script>
@endsection
