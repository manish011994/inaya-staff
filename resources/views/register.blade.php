<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <link rel="icon" href="{{ asset('/images/inayaico.png') }}" sizes="32x32"/>
    <link rel="icon" href="{{ asset('/images/inayaico.png') }}" sizes="192x192"/>
    <link rel="apple-touch-icon" href="{{ asset('/images/inayaico.png') }}"/>
    <title>Register &mdash; Inaya Portal</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ URL::to('/css/style.css') }}"/>
    <link rel="stylesheet" href="{{ URL::to('/css/components.css') }}"/>
</head>

<body>
<div id="app">
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div
                    class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                    <div class="login-brand">
                        <img src="{{ URL::to('/images/INAYA.svg') }}" alt="logo" width="250"/>
                    </div>

                    <div class="card card-primary">
                        <div class="card-header"><h4>Register</h4></div>

                        <div class="card-body">
                            <form id="register_form" method="POST" action="{{ URL::to('/register') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="name">Contact Name</label>
                                    </div>
                                    <div class="form-group col-8">
                                        <input id="name" type="text" maxlength="100"
                                               class="form-control form-control-sm" name="name" required autofocus>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="phone">Contact Number</label>
                                    </div>
                                    <div class="form-group col-2">
                                        <input id="phone_ext" type="text" class="form-control form-control-sm"
                                               name="phone_ext" value="+971" readonly disabled>
                                    </div>
                                    <div class="form-group col-6">
                                        <input id="phone" type="number" min="111111111" max="999999999" maxlength="10"
                                               class="form-control form-control-sm" name="phone" required>
                                        <label for="phone" id="phone_error"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="email_id">Email ID</label>
                                    </div>
                                    <div class="form-group col-8">
                                        <input id="email_id" type="email" maxlength="150"
                                               class="form-control form-control-sm" name="email_id" required>
                                        <label for="email_id" id="email_error"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="location">Location</label>
                                    </div>
                                    <div class="form-group col-8">
                                        <select id="location" class="form-control" name="location" required>
                                            <option value="">Please select location</option>
                                            <option value="Dubai">Dubai</option>
                                            <option value="Abudhabi">Abudhabi</option>
                                            <option value="Sharjah">Sharjah</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="secondary_name">Secondary Contact Name</label>
                                    </div>
                                    <div class="form-group col-8">
                                        <input id="secondary_name" type="text" maxlength="100"
                                               class="form-control form-control-sm" name="secondary_name" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="secondary_number">Secondary Contact Number</label>
                                    </div>
                                    <div class="form-group col-2">
                                        <input id="phone_ext_2" type="text" class="form-control form-control-sm"
                                               name="phone_ext_2" value="+971" readonly disabled>
                                    </div>
                                    <div class="form-group col-6">
                                        <input id="secondary_number" type="number" min="111111111" max="999999999"
                                               maxlength="9"
                                               class="form-control form-control-sm" name="secondary_number" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="secondary_email">Secondary Email ID</label>
                                    </div>
                                    <div class="form-group col-8">
                                        <input id="secondary_email" type="email" maxlength="150"
                                               class="form-control form-control-sm" name="secondary_email" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="password">Password</label>
                                    </div>
                                    <div class="form-group col-8">
                                        <input id="password" autocomplete="off" type="password" minlength="8"
                                               maxlength="30"
                                               class="form-control form-control-sm" name="password" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label for="password2">Confirm Password</label>
                                    </div>
                                    <div class="form-group col-8">
                                        <input id="password2" autocomplete="off" type="password" minlength="8"
                                               maxlength="30"
                                               class="form-control form-control-sm" name="password2" required>
                                    </div>
                                </div>
                                <div id="response_msg" class="alert mb-2"></div>
                                <div class="form-group">
                                    <button type="submit" id="submit_register"
                                            class="btn btn-success btn-lg btn-block font-size-20">
                                        Register
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="simple-footer">
                        Copyright &copy; {{ date('Y') }} Powered by <a
                            href="https://onlinemarketingdubai.ae">Redberries</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{ URL::to('/js/inaya.js') }}"></script>

<script src="{{ asset('/js/jquery.validate.min.js') }}"></script>

<!-- Template JS File -->
<script src="{{ URL::to('/js/scripts.js') }}"></script>
<script src="{{ URL::to('/js/custom.js') }}"></script>

<script type="text/javascript">
    $(function () {
        $("#register_form").submit(function (e) {
            let $form = $(this);
            if ($("#register_form").valid()) {
                $("#response_msg").removeClass('alert-danger').removeClass('alert-success').html("");
                var pwd = $("#password").val();
                var pwd1 = $("#password2").val();
                if (pwd !== pwd1) {
                    $("#response_msg").addClass('alert-danger').html("<p>Passwords not matched.</p>");
                    return false;
                }
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                $.ajax({
                    url: formURL,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    beforeSend: function () {
                        $("#submit_register").html("Please wait <i class='fa fa-spinner fa-spin'></i>").prop("disabled", "true");
                    },
                    success: function (response) {
                        if (response.success) {
                            $form[0].reset();
                            $("#response_msg").addClass('alert-success').html(response.msg);
                            setTimeout(function () {
                                window.location.href = "{{ URL::to('/verify-otp') }}";
                            }, 3000);
                        } else {
                            $("#response_msg").addClass('alert-danger').html(response.msg);
                        }
                    },
                    error: function () {
                        $("#response_msg").addClass('alert-danger').html("<p>Some error occurred at the Server.</p>");
                    }
                    ,
                    complete: function () {
                        $("#submit_register").html("Register").removeAttr("disabled");
                    }
                });
                e.preventDefault();
            }
        });

        $("#phone").blur(function () {
            $.ajax({
                url: '{{ URL::to('/is-phone-registered') }}',
                type: "POST",
                data: {'_token': '{{ csrf_token() }}', 'phone': $("#phone").val()},
                dataType: "json",
                beforeSend: function () {
                    $("#phone_error").removeClass("text-danger").removeClass("text-success").html("");
                },
                success: function (response) {
                    if (response.success) {
                        $("#phone_error").addClass("text-danger").html(response.msg);
                    }
                }
            });
        });


        $("#email_id").blur(function () {
            $.ajax({
                url: '{{ URL::to('/is-email-registered') }}',
                type: "POST",
                data: {'_token': '{{ csrf_token() }}', 'email': $("#email_id").val()},
                dataType: "json",
                beforeSend: function () {
                    $("#email_error").removeClass("text-danger").removeClass("text-success").html("");
                },
                success: function (response) {
                    if (response.success) {
                        $("#email_error").addClass("text-danger").html(response.msg);
                    }
                }
            });
        });
    });
</script>
</body>
</html>
