@extends('app')
@section('title') Inaya Portal - Profile Update @endsection

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Staff - Profile Update</h1>
            </div>

            <div class="section-body">

                <form id="profile_update" method="post" action="{{ URL::to('/profile-update') }}">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="name">Contact Name</label>
                                        <input id="name" name="name" type="text"
                                               class="form-control form-control-sm" value="{{ $userDetails->name }}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="email_id">Email ID</label>
                                        <input id="email_id" name="email_id" type="email"
                                               class="form-control form-control-sm" value="{{ $userDetails->email_id }}"
                                               readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="row">
                                        <div class="form-group col-12 mb-0">
                                            <label for="phone">Phone</label>
                                        </div>
                                        <div class="form-group col-2">
                                            <input id="phone_ext" type="text" class="form-control form-control-sm"
                                                   name="phone_ext" value="+971" readonly disabled>
                                        </div>
                                        <div class="form-group col-10">
                                            <input id="phone" type="number" min="111111111" max="999999999" maxlength="10"
                                                   class="form-control form-control-sm" name="phone"
                                                   value="{{ $userDetails->phone }}" readonly disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @php
                                $locations = ['Dubai', 'Abudhabi', 'Sharjah'];
                            @endphp
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <select id="location" class="form-control" name="location" required>
                                            <option>Please select location</option>
                                            @foreach ($locations as $location)
                                                <option
                                                    value="{{ $location }}" {{ $userDetails->location == $location ? 'selected' : ''}}>{{ $location }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="secondary_name">Secondary Contact Name</label>
                                        <input id="secondary_name" name="secondary_name" type="text"
                                               class="form-control form-control-sm"
                                               value="{{ $userDetails->secondary_name }}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="row">
                                        <div class="form-group col-12 mb-0">
                                            <label for="secondary_number">Secondary Contact Number</label>
                                        </div>
                                        <div class="form-group col-2">
                                            <input id="phone_ext" type="text" class="form-control form-control-sm"
                                                   name="phone_ext" value="+971" readonly disabled>
                                        </div>
                                        <div class="form-group col-10">
                                            <input id="secondary_number" type="number" min="111111111" max="999999999" maxlength="10"
                                                   class="form-control form-control-sm" name="secondary_number"
                                                   value="{{ $userDetails->secondary_number }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="secondary_email">Secondary Contact Email</label>
                                        <input id="secondary_email" name="secondary_email"
                                               type="email" class="form-control form-control-sm"
                                               value="{{ $userDetails->secondary_email }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="alert" id="response_msg">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-whitesmoke text-right">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-end">
                                    <button class="btn btn-success mr-1" type="submit" id="update_profile"> Update
                                    </button>
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('js_content')
    <script type="text/javascript">
        $(function () {
            $("#profile_update").submit(function (e) {
                $("#response_msg").removeClass('alert-danger').removeClass('alert-success');
                let $form = $(this);
                if ($("#profile_update").valid()) {
                    var postData = $(this).serializeArray();
                    var formURL = $(this).attr("action");
                    $.ajax({
                        url: formURL,
                        type: "POST",
                        data: postData,
                        dataType: "json",
                        beforeSend: function () {
                            $("#update_profile").html("Please wait <i class='fa fa-spinner fa-spin'></i>").prop("disabled", "true");
                        },
                        success: function (response) {
                            if (response.success) {
                                $("#response_msg").addClass('alert-success').html(response.msg);
                            } else {
                                $("#response_msg").addClass('alert-danger').html(response.msg);
                            }
                        },
                        error: function () {
                            $("#response_msg").addClass('alert-danger').html("<p>Some error occurred at the Server.</p>");
                        }
                        ,
                        complete: function () {
                            $("#update_profile").html("Update").removeAttr("disabled");
                        }
                    });
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection
