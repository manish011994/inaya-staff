<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <link rel="icon" href="{{ asset('/images/inayaico.png') }}" sizes="32x32"/>
    <link rel="icon" href="{{ asset('/images/inayaico.png') }}" sizes="192x192"/>
    <link rel="apple-touch-icon" href="{{ asset('/images/inayaico.png') }}"/>
    <title>Login - Inaya Portal</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/components.css') }}">
</head>

<body>
<div id="app">
    <section class="section">
        <div class="container mt-2">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                    <div class="login-brand">
                        <img src="{{ URL::to('/images/INAYA.svg') }}" alt="logo" width="250"/>
                    </div>

                    <div class="card card-primary">
                        <div class="card-header"><h4>Login</h4></div>

                        <div class="card-body">
                            <form id="login_form" method="POST" action="{{ URL::to('/login') }}"
                                  class="needs-validation" autocomplete="off" novalidate="">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="phone">Phone</label>
                                    </div>
                                    <div class="form-group col-3">
                                        <input id="phone_ext" type="text" class="form-control form-control-sm"
                                               name="phone_ext" value="+971" readonly disabled>
                                    </div>
                                    <div class="form-group col-9">
                                        <input id="phone" type="number" min="111111111" max="999999999" maxlength="10"
                                               class="form-control form-control-sm" name="phone" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="d-block">
                                        <label for="password" class="control-label">Password</label>
                                    </div>
                                    <input id="password" type="password" class="form-control" name="password"
                                           placeholder="******" autocomplete="off" tabindex="2" required>
                                </div>
                                <div class="g-recaptcha" data-sitekey="{{ getenv('GOOGLE_RECAPTCHA_SITE_KEY') }}"></div>
                                <div id="response_msg" class="alert mb-2"></div>
                                <div class="form-group mt-1">
                                    <button type="submit" id="submit_login"
                                            class="btn btn-success btn-lg btn-block font-weight-bold font-size-20"
                                            tabindex="4">
                                        Login
                                    </button>
                                </div>
                            </form>
                            <div class="mt-4 mb-1">
                                <a href="{{ URL::to('/forgot-password') }}" class="text-small">
                                    Forgot Password?
                                </a>
                            </div>
                            <div class="mt-1 mb-3">
                                <a href="{{ URL::to('/register') }}" class="text-small">
                                    Not a user? Register
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="simple-footer">
                        Copyright &copy; {{ date('Y') }} Powered by <a
                            href="https://onlinemarketingdubai.ae">Redberries</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{ asset('/js/inaya.js') }}"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<!-- JS Libraies -->
<script src="{{ asset('/js/jquery.validate.min.js') }}"></script>
<!-- Page Specific JS File -->

<!-- Template JS File -->
<script src="{{ URL::to('/js/scripts.js') }}"></script>
<script src="{{ URL::to('/js/custom.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $("#login_form").submit(function (e) {
            $("#response_msg").removeClass('alert-danger').removeClass('alert-success');
            let $form = $(this);
            if ($("#login_form").valid()) {
                let captcha = grecaptcha.getResponse();
                if (captcha == "") {
                    $("#response_msg").addClass('alert-danger').html("<p>Please check the reCaptcha.</p>");
                    return false;
                }
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                $.ajax({
                    url: formURL,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    beforeSend: function () {
                        $("#submit_login").html("Please wait <i class='fa fa-spinner fa-spin'></i>").prop("disabled", "true");
                    },
                    success: function (response) {
                        if (response.success) {
                            $form[0].reset();
                            $("#response_msg").addClass('alert-success').html(response.msg);
                            window.location.reload();
                        } else {
                            $("#response_msg").addClass('alert-danger').html(response.msg);
                        }
                    },
                    error: function () {
                        $("#response_msg").addClass('alert-danger').html("<p>Some error occurred at the Server.</p>");
                    }
                    ,
                    complete: function () {
                        grecaptcha.reset();
                        $("#submit_login").html("Login").removeAttr("disabled");
                    }
                });
                e.preventDefault(); //STOP default action
            }
        });
    });
</script>
</body>
</html>
