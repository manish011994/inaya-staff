@extends('app')
@section('title') Inaya Portal - List Update Request @endsection
@section('css_content')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
@endsection

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Staff - List Update Request</h1>
            </div>

            <div class="section-body">

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 text-right">
                                <label for="job_status">Filter By:</label>
                                <select class="" name="job_status" id="job_status">
                                    <option value="">Please select Job Type</option>
                                    @if(!empty($statusTypes) && count($statusTypes))
                                        @foreach($statusTypes as $status)
                                            <option value="{{ $status->status }}">{{ $status->status }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <hr/>
                        <div class="table-responsive">
                            <table id="list_request" width="100%"
                                   class="table table-sm table-striped table-hover font-size-12">
                                <thead>
                                <tr>
                                    <th>Complaint No</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>SWO</th>
                                    <th>Description</th>
                                    <th>Unit ID</th>
                                    <th>S. Name</th>
                                    <th>S. Phone</th>
                                    <th>Category</th>
                                    <th>Status</th>
                                    <th>Created Date</th>
                                    <th>Created Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--@if(!empty($updateRequests) && count($updateRequests))
                                    @foreach($updateRequests as $request)
                                        <tr class="data-row {{ str_replace(" ", "_", $request->status) }}">
                                            <td>{{ $request->complaint_no }}</td>
                                            <td>{{ $request->swo_type }}</td>
                                            <td>{{ $request->swo_description }}</td>
                                            <td>{{ $request->unit_id }}</td>
                                            <td>{{ $request->category }}</td>
                                            <td>{{ $request->status }}</td>
                                            <td>{{ $request->created_date }}</td>
                                            <td>{{ $request->created_time }}</td>
                                        </tr>
                                    @endforeach
                                @endif--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('js_content')
    <script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#list_request').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ URL::to('/list-request/get-update-requests') }}",
                    "data": function (d) {
                        d.statusFilter = $("#job_status").val()
                    }
                },
                "pageLength": 50,
                "scrollY": "400px",
                "scrollCollapse": true,
                "order": [6, 'desc'],
                "dom": '<"top"lpf>rt<"bottom"ip><"clear">',
                "lengthMenu": [[50, 100, 250, 500, 1000, "All"], [50, 100, 250, 500, 1000, "All"]],
                "columns": [
                    {data: 'complaint_no'},
                    {data: 'customer_name'},
                    {data: 'customer_phone'},
                    {data: 'swo_type'},
                    {data: 'swo_description'},
                    {data: 'unit_id'},
                    {data: 'secondary_name'},
                    {data: 'secondary_phone_no'},
                    {data: 'category'},
                    {data: 'status'},
                    {data: 'created_date'},
                    {data: 'created_time'},
                ]
            });

            $("#job_status").change(function () {
                $('#list_request').DataTable().clear().destroy();
                $('#list_request').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ URL::to('/list-request/get-update-requests') }}",
                        "data": function (d) {
                            d.statusFilter = $("#job_status").val()
                        }
                    },
                    "pageLength": 50,
                    "scrollY": "400px",
                    "scrollCollapse": true,
                    "order": [6, 'desc'],
                    "dom": '<"top"lpf>rt<"bottom"ip><"clear">',
                    "lengthMenu": [[50, 100, 250, 500, 1000, "All"], [50, 100, 250, 500, 1000, "All"]],
                    "columns": [
                        {data: 'complaint_no'},
                        {data: 'customer_name'},
                        {data: 'customer_phone'},
                        {data: 'swo_type'},
                        {data: 'swo_description'},
                        {data: 'unit_id'},
                        {data: 'secondary_name'},
                        {data: 'secondary_phone_no'},
                        {data: 'category'},
                        {data: 'status'},
                        {data: 'created_date'},
                        {data: 'created_time'},
                    ]
                });
            });
        });
    </script>
@endsection
