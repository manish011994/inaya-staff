@extends('app')
@section('title') Inaya Portal - Manage Phone Change Request @endsection
@section('css_content')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Manage - Phone Change Request</h1>
            </div>

            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="manage_request" width="100%"
                                   class="table table-sm table-striped table-hover font-size-12">
                                <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Old Phone</th>
                                    <th>New Phone</th>
                                    <th>Primary Name</th>
                                    <th>Primary Email</th>
                                    <th>Secondary Name</th>
                                    <th>Secondary Email</th>
                                    <th>Secondary Phone</th>
                                    <th>Unit Id</th>
                                    <th>Created Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $key=>$request)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $request->old_phone }}</td>
                                        <td>{{ $request->new_phone }}</td>
                                        <td>{{ $request->primary_name }}</td>
                                        <td>{{ $request->primary_email }}</td>
                                        <td>{{ $request->secondary_name }}</td>
                                        <td>{{ $request->secondary_email }}</td>
                                        <td>{{ $request->secondary_phone }}</td>
                                        <td>{{ $request->unit_id }}</td>
                                        <td>{{ date('Y-m-d', strtotime($request->created_at)) }}</td>
                                        <td class="hideAction">
                                            <a data-id="{{ $request->id }}"
                                               class="btn btn-success btn-sm approveRequest"
                                               href="{{ URL::to('/phone-change-request/'.$request->id.'/approve') }}">Approve</a>
                                            <a data-id="{{ $request->id }}" class="btn btn-danger btn-sm rejectRequest"
                                               id=""
                                               href="{{ URL::to('/phone-change-request/'.$request->id.'/reject') }}">Reject</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer bg-whitesmoke text-right">
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('js_content')
    <script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $("#manage_request").DataTable({
                "pageLength": 50,
                "scrollY": "400px",
                "scrollCollapse": true,
                "dom": '<"top"lpf>rt<"bottom"ip><"clear">',
                "lengthMenu": [[50, 100, 250, 500, 1000, "All"], [50, 100, 250, 500, 1000, "All"]]
            });
        });

        $('.approveRequest').click(function () {
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                data: {"_token": '{{csrf_token()}}'},
                dataType: "json",
                success: function (response) {
                    if (response.result == "success") {
                        alert("Successfully approved and phone updated.");
                        window.location.reload();
                    }
                },
                error: function () {
                    window.location.reload();
                },
            });
            return false;
        });

        $('.rejectRequest').click(function () {
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                data: {"_token": '{{csrf_token()}}'},
                dataType: "json",
                success: function (response) {
                    if (response.result == "success") {
                        alert("Rejected the request.");
                        window.location.reload();
                    }
                },
                error: function () {
                    window.location.reload();
                },
            });
            return false;
        });
    </script>
@endsection
