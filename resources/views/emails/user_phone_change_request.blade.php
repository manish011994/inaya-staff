<html>
<body>
<p>Dear {{ $phoneUpdateRequest['primary_name'] }},</p>
<p>
    Your phone number change request is completed.<br/>
    Phone number is changed to {{ $phoneUpdateRequest['new_phone'] }}.
</p>
</body>
</html>
