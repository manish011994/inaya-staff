@extends('app')
@section('title') Inaya Portal - New Request @endsection

@section('content')
    <!-- Main Content -->
    <div class="main-content container">
        <section class="section">
            <div class="section-header">
                <h1>Customer - Place Request</h1>
            </div>

            <div class="section-body">

                <form id="new_request" method="post" action="{{ URL::to('/new_cust_request') }}">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-1">
                                <div class="col-3 text-left">Employee Name:</div>
                                <div class="col-3 text-left">{{ $userDetails->name }}</div>
                                <div class="col-3 text-left">Employee Phone:</div>
                                <div class="col-3 text-left">+971 {{ $userDetails->phone }}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-3 text-left">Date:</div>
                                <div class="col-3 text-left">{{ date('d-m-Y') }}</div>
                                <div class="col-3 text-left">Time:</div>
                                <div class="col-3 text-left">{{date('H:i A')}}</div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="form-group">
                                                <label for="priority">Priority</label>
                                                <select class="form-control form-control-sm" name="priority" id="priority"
                                                        required>
                                                    <option value="">Please select priority</option>
                                                    <option value="Emergency">Emergency</option>
                                                    <option value="Urgent">Urgent</option>
                                                    <option value="Routine">Routine</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="form-group">
                                                <label for="nature_of_complaint">Nature of Complaint</label>
                                                <select class="form-control form-control-sm" name="nature_of_complaint" id="nature_of_complaint"
                                                        required>
                                                    <option value="">Please select Nature of Complaint</option>
                                                    @foreach($natureOfComplaints as $complaint)
                                                        <option value="{{ $complaint->id }}">{{ $complaint->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="priority">Unit ID</label>
                                        <select class="form-control form-control-sm" name="unit_id" id="unit_id"
                                                required>
                                            <option value="">Please select Unit Id</option>
                                            @foreach($unitIds as $unit)
                                                <option value="{{ $unit }}">{{ $unit }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="work_order_category">Work Order Category</label>
                                        <select class="form-control form-control-sm" name="work_order_category"
                                                id="work_order_category" data-token="{{ csrf_token() }}"
                                                required>
                                            <option value="">Please select category</option>
                                            @if(!empty($categories) && count($categories))
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->category }}">{{ $category->category }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="failure_class">Failure Class</label>
                                        <select class="form-control form-control-sm" name="failure_class"
                                                id="failure_class"
                                                required>
                                            <option value="">Please select failure class</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="description">Brief Description</label>
                                        <textarea id="description" name="description" required
                                                  class="form-control form-control-sm"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="secondary_contact_name">Secondary Contact Name</label>
                                        <input id="secondary_contact_name" name="secondary_contact_name" type="text"
                                               class="form-control form-control-sm" value="{{ $userDetails->secondary_name }}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="form-group col-12 mb-0">
                                            <label for="secondary_contact_phone">Secondary Contact Phone</label>
                                        </div>
                                        <div class="form-group col-2">
                                            <input id="phone_ext" type="text" class="form-control form-control-sm"
                                                   name="phone_ext" value="+971" readonly disabled>
                                        </div>
                                        <div class="form-group col-10">
                                            <input id="secondary_contact_phone" type="number" min="111111111" max="999999999" maxlength="10"
                                                   class="form-control form-control-sm" name="secondary_contact_phone"
                                                   value="{{ $userDetails->secondary_number }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="secondary_contact_email">Secondary Contact Email</label>
                                        <input id="secondary_contact_email" name="secondary_contact_email"
                                               type="email" class="form-control form-control-sm"
                                               value="{{ $userDetails->secondary_email }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 align-self-center">
                                    <div class="alert" id="response_msg">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-whitesmoke text-center">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <button class="btn btn-success mr-1" type="submit" id="register"> Submit</button>
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
