@extends('app')
@section('title') Inaya Portal - Manage @endsection

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>
                    @if(Request::url() == URL::to('/nature-of-complaints/add'))
                        Add
                    @else
                        Update
                    @endif
                    - Nature of Complaints</h1>
            </div>

            <div class="section-body">

                <form id="add_noc" method="post" action="{{ Request::url() }}">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="name">Nature of Complaint</label>
                                        <input id="name" name="name" type="text"
                                               class="form-control form-control-sm"
                                               placeholder="IT Request"
                                               @if(Request::url() != URL::to('/nature-of-complaints/add'))
                                               value="{{ $noc->name }}"
                                               @endif
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="initials">Initial</label>
                                        <input id="initials" name="initials" type="text"
                                               class="form-control form-control-sm"
                                               placeholder="IT"
                                               @if(Request::url() != URL::to('/nature-of-complaints/add'))
                                               value="{{ $noc->initials }}"
                                               @endif
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="alert" id="response_msg">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-whitesmoke text-right">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-end">
                                    <button class="btn btn-success mr-1" type="submit" id="create_noc">
                                        @if(Request::url() == URL::to('/nature-of-complaints/add'))
                                            Create
                                        @else
                                            Update
                                        @endif
                                    </button>
                                    <a class="btn btn-secondary" href="{{ URL::to('/nature-of-complaints/') }}">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('js_content')
    <script type="text/javascript">
        $(function () {
            $("#add_noc").submit(function (e) {
                $("#response_msg").removeClass('alert-danger').removeClass('alert-success');
                let $form = $(this);
                if ($("#add_noc").valid()) {
                    var postData = $(this).serializeArray();
                    var formURL = $(this).attr("action");
                    $.ajax({
                        url: formURL,
                        type: "POST",
                        data: postData,
                        dataType: "json",
                        beforeSend: function () {
                            $("#create_noc").html("Please wait <i class='fa fa-spinner fa-spin'></i>").prop("disabled", "true");
                        },
                        success: function (response) {
                            if (response.success) {
                                @if(Request::url() == URL::to('/nature-of-complaints/add'))
                                    $form[0].reset();
                                @endif
                                $("#response_msg").addClass('alert-success').html(response.msg);
                            } else {
                                $("#response_msg").addClass('alert-danger').html(response.msg);
                            }
                        },
                        error: function () {
                            $("#response_msg").addClass('alert-danger').html("<p>Some error occurred at the Server.</p>");
                        },
                        complete: function () {
                            @if(Request::url() == URL::to('/nature-of-complaints/add'))
                            $("#create_noc").html("Add").removeAttr("disabled");
                            @else
                            $("#create_noc").html("Update").removeAttr("disabled");
                            @endif
                        }
                    });
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection
