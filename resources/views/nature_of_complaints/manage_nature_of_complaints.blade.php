@extends('app')
@section('title') Inaya Portal - Manage @endsection
@section('css_content')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Manage - Nature of Complaints</h1>
            </div>

            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ URL::to('/nature-of-complaints/add') }}" class="btn btn-success float-right mb-2">Add New</a>
                        <div class="table-responsive">
                            <table id="manage_nocs" width="100%"
                                   class="table table-sm table-striped table-hover font-size-12">
                                <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Name</th>
                                    <th>Initials</th>
                                    <th>Created Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($nocs as $key=>$noc)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $noc->name }}</td>
                                        <td>{{ $noc->initials }}</td>
                                        <td>{{ date('Y-m-d', strtotime($noc->created_at)) }}</td>
                                        <td>
                                            <a class="btn btn-warning btn-sm"
                                               href="{{ URL::to('/nature-of-complaints/edit/'.$noc->id) }}">Edit</a>
                                            <a class="btn btn-danger btn-sm delete_noc"
                                               href="{{ URL::to('/nature-of-complaints/delete/'.$noc->id) }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer bg-whitesmoke text-right">
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('js_content')
    <script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $("#manage_nocs").DataTable({
                "pageLength": 50,
                "scrollY": "400px",
                "scrollCollapse": true,
                "dom": '<"top"lpf>rt<"bottom"ip><"clear">',
                "lengthMenu": [[50, 100, 250, 500, 1000, "All"], [50, 100, 250, 500, 1000, "All"]]
            });
        });

        $('.delete_noc').click(function () {
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                data: {"_token": '{{csrf_token()}}'},
                success: function (data) {
                    window.location.reload();
                },
                error: function () {
                    alert("Some Error Occurred");
                },
            });
            return false;
        });
    </script>
@endsection
