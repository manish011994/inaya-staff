@extends('app')
@section('title') Inaya Portal - Manage @endsection
@section('css_content')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Manage - Failure Class - {{ $categoryName }}</h1>
            </div>

            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ URL::to('/category-failure-class/') }}" class="btn btn-light mb-2">Back to Categories</a>
                        <a href="{{ URL::to('/category-failure-class/failure-class/'.$categoryId."/add") }}" class="btn btn-success float-right mb-2">Add New</a>
                        <div class="table-responsive">
                            <table id="manage_failure_class" width="100%"
                                   class="table table-sm table-striped table-hover font-size-12">
                                <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Failure Class Name</th>
                                    <th>Created Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($failureClasses as $key=>$class)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $class->category }}</td>
                                        <td>{{ date('Y-m-d', strtotime($class->created_at)) }}</td>
                                        <td>
                                            <a class="btn btn-warning btn-sm"
                                               href="{{ URL::to('/category-failure-class/failure-class/'.$categoryId.'/edit/'.$class->id) }}">Edit</a>
                                            <a class="btn btn-danger btn-sm delete_category"
                                               href="{{ URL::to('/category-failure-class/failure-class/'.$categoryId.'/delete/'.$class->id) }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer bg-whitesmoke text-right">
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('js_content')
    <script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $("#manage_failure_class").DataTable({
                "pageLength": 50,
                "scrollY": "400px",
                "scrollCollapse": true,
                "dom": '<"top"lpf>rt<"bottom"ip><"clear">',
                "lengthMenu": [[50, 100, 250, 500, 1000, "All"], [50, 100, 250, 500, 1000, "All"]]
            });
        });

        $('.delete_category').click(function () {
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                data: {"_token": '{{csrf_token()}}'},
                success: function (data) {
                    window.location.reload();
                },
                error: function () {
                    alert("Some Error Occurred");
                },
            });
            return false;
        });
    </script>
@endsection
