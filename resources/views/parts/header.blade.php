<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
    <a href="{{ URL::to('/') }}" class="navbar-brand sidebar-gone-hide"></a>
    <div class="nav-collapse">
        <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <ul class="navbar-nav">
            <li class="nav-item"><a href="{{ URL::to('/') }}" class="nav-link"><img
                        src="{{ asset('/images/INAYA.svg') }}"
                        width="150px"/></a></li>
            <li class="nav-item {{ Request::is('new-request') ? 'active' : '' }}"><a
                    href="{{ URL::to('/new-request') }}" class="nav-link">New Request</a></li>
            {{--<li class="nav-item {{ Request::is('list-request') ? 'active' : '' }}"><a
                    href="{{ URL::to('/list-request') }}" class="nav-link">List Request</a></li>--}}
            <li class="dropdown nav-item {{ Request::is('list-request/*') ? 'active' : '' }}"><a
                    href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">List Request</a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ URL::to('/list-request/placed-requests') }}"
                           class="dropdown-item {{ (request()->is('list-request/placed-requests')) ? 'active' : '' }}">
                            Placed Requests</a></li>
                    <li><a href="{{ URL::to('/list-request/update-requests') }}"
                           class="dropdown-item {{ (request()->is('list-request/update-requests')) ? 'active' : '' }}">
                            Update Requests</a></li>
                </ul>
            </li>
            @if(session()->has('user_details') && session()->get('user_details')->is_admin == 'Y')
                <li class="dropdown nav-item"><a
                        href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Manage</a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::to('/nature-of-complaints') }}"
                               class="dropdown-item {{ (request()->is('nature-of-complaints')) ? 'active' : '' }}">
                                Manage Nature of Complaint</a></li>
                        <li><a href="{{ URL::to('/category-failure-class') }}"
                               class="dropdown-item {{ (request()->is('category-failure-class')) ? 'active' : '' }}">
                                Manage Category/Failure</a></li>
                    </ul>
                </li>

                <li class="nav-item {{ Request::is('phone-change-request') ? 'active' : '' }}">
                    <a href="{{ URL::to('/phone-change-request') }}" class="nav-link">Phone Change Request</a></li>
            @endif
            <li class="nav-item {{ Request::is('contact-us') ? 'active' : '' }}"><a href="{{ URL::to('/contact-us') }}"
                                                                                    class="nav-link">Contact Us</a></li>
            <li class="nav-item"><a
                    href="https://www.facebook.com/InayaFM" class="nav-link no-padding"><i class="fab fa-facebook"></i></a>
            </li>
            <li class="nav-item"><a
                    href="https://www.linkedin.com/company/inaya-facilities-management-services-a-member-of-belhasa-group-/"
                    class="nav-link no-padding"><i class="fab fa-linkedin"></i></a></li>
            <li class="nav-item"><a
                    href="https://www.youtube.com/channel/UC2Siy-8xKf4IJocJR-M1Mrg" class="nav-link no-padding"><i
                        class="fab fa-youtube"></i></a></li>
        </ul>
    </div>
    <ul class="navbar-nav navbar-right ml-auto">
        <li class="dropdown"><a href="#" data-toggle="dropdown"
                                class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <div class="d-sm-none d-lg-inline-block">
                    Hi, {{ session()->has('user_details') ? session()->get('user_details')->name : "" }}</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="{{URL::to('/profile-update')}}"
                   class="dropdown-item has-icon {{ Request::is('profile-update') ? 'active' : '' }}">
                    <i class="far fa-user"></i> Profile Update
                </a>
                <a href="{{ URL::to('/change-password') }}"
                   class="dropdown-item has-icon {{ Request::is('change-password') ? 'active' : '' }}">
                    <i class="fas fa-cog"></i> Change Password
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ URL::to('/logout') }}" class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
