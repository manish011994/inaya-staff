@extends('app')
@section('title')Inaya Portal - Contact Us @endsection

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Customer - Contact Us</h1>
            </div>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>INAYA Facilities Management Services LLC</h4>
                                <h5>A Member of Belhasa group</h5>
                                <p class="font-size-16">
                                    NAM Building No 1,<br/>
                                    Jebel Ali,<br/>
                                    PO. Box 87074<br/>
                                    Dubai, UAE<br/>
                                    T: <a href="tel:04-882-7001" title="Phone"> 04 882 7001</a><br/>
                                    F: 04 882 7002<br/>
                                    w: <a href="//www.inaya.ae" title="Website">www.inaya.ae</a>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6>COMPANY INFO</h6>
                                        <ul class="footer-links">
                                            <li><a href="//www.inaya.ae/facilities-management-company-dubai/">About
                                                    Us</a></li>
                                            <li><a href="//www.inaya.ae/qhse/">QHSE</a></li>
                                            <li><a href="//www.inaya.ae/sustainability/">Sustainability</a></li>
                                            <li><a href="//www.inaya.ae/careers/">Careers</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6">
                                        <h6>OUR SERVICES</h6>
                                        <ul class="footer-links">
                                            <li><a href="//www.inaya.ae/integrated-fm-services-dubai/">Integrated
                                                    FM Services</a></li>
                                            <li><a href="//www.inaya.ae/facilities-management-services-dubai/">Hard
                                                    Services</a></li>
                                            <li><a href="//www.inaya.ae/soft-services/">Soft Services</a></li>
                                            <li><a href="//www.inaya.ae/project-management/">Project
                                                    Management</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light">
                        <div class="row">
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-success btn-lg font-size-16 get_in_touch" onclick="window.location.href = '//www.inaya.ae/contact-us/';">
                                    GET IN TOUCH
                                </button>
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <h6>CUSTOMER SERVICE</h6>
                                        <a href="tel:+971-(4)-815-7300" title="Customer Service">+971 (4) 815 7300</a>
                                    </div>
                                    <div class="col-sm-4">
                                        <h6>BUSINESS ENQUIRY</h6>
                                        <a href="mailto:bd@inaya.ae" title="Business Enquiry">BD@inaya.ae</a>
                                    </div>
                                    <div class="col-sm-4">
                                        <h6>GENERAL ENQUIRY</h6>
                                        <a href="mailto:info@inaya.ae" title="General Enquiry">info@inaya.ae</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
